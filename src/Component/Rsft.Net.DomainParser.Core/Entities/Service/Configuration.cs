﻿// <copyright file="Configuration.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Core.Entities.Service
{
    /// <summary>
    /// The configuration.
    /// </summary>
    public sealed class Configuration
    {
        /// <summary>
        ///     Gets or sets the TLD public rules URL. Default is https://publicsuffix.org/list/effective_tld_names.dat.
        /// </summary>
        public string TldPublicRulesUrl { get; set; }

        /// <summary>
        ///     Gets or sets the TLD public fetch timeout (in milliseconds).
        /// </summary>
        /// <value>
        ///     The TLD public fetch timeout (in milliseconds). Default is 8000.
        /// </value>
        public int TldPublicFetchTimeout { get; set; }
    }
}