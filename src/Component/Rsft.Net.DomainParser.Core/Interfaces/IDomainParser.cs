﻿// <copyright file="IDomainParser.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Core.Interfaces
{
    /// <summary>
    /// The DomainParser interface.
    /// </summary>
    /// <typeparam name="TResponse">
    /// The type of the response.
    /// </typeparam>
    public interface IDomainParser<TResponse>
    {
        /// <summary>
        /// The parse.
        /// </summary>
        /// <param name="domainString">
        /// The domain string.
        /// </param>
        /// <returns>
        /// The TResponse.
        /// </returns>
        TResponse Parse(string domainString);

        /// <summary>
        /// The try parse.
        /// </summary>
        /// <param name="domainString">
        /// The domain string.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool TryParse(string domainString, out TResponse response);
    }
}